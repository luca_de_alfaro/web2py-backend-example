

def entry():
    redirect(URL('default', 'user', args=['login'], vars=dict(_next='/backend/uman/yourein')))

@auth.requires_login()
def yourein():
    """We get here once we log in.  We must return to the user their access token."""
    return dict(user_token=auth.user.user_token)

OK_FIELDS = ['last_name', 'first_name', 'email']

def me():
    """Info on yourself.  Input:
    request.vars.user_token
    """
    row = db(db.auth_user.user_token == request.vars.user_token).select().first().as_dict()
    d = { k: row[k] for k in OK_FIELDS }
    return response.json(d)


