


def store_ui():
    """Stores user info.
    Input:
    request.vars.user_id
    request.vars.user_info
    """

    db.userinfo.update_or_insert((db.userinfo.user_id == request.vars.user_id),
                                 user_id = request.vars.user_id,
                                 user_info = request.vars.user_info
                                 )
    return "ok"

def read_ui():
    """Gets user info. Input:
        request.vars.user_id
    """
    row = db(db.userinfo.user_id == request.vars.user_id).select().first().as_dict()
    return response.json(row)
